#!/bin/bash
# SPDX-License-Identifier: LGPL-2.1+
# ~~~
#   runtest.sh of libtevent
#   Description: Tests for libtevent
#
#   Author: Susant Sahani <susant@redhat.com>
#   Copyright (c) 2018 Red Hat, Inc.
# ~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="libtevent"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "cp test-libtevent /usr/bin/"
    rlPhaseEnd

    rlPhaseStartTest
        rlLog "Starting libtevent tests ..."
        rlRun "/usr/bin/test-libtevent"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm /usr/bin/test-libtevent"
        rlLog "libtevent tests done"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

rlGetTestState
